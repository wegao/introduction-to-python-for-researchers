<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-161947002-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-161947002-3');
</script>

</script>

# Introduction to Python

This workshop is designed for those trying to explore and learn about Python for the very first time. The content is suitable for a range of learners - from those who have never programmed before, to those who are already proficient in other coding languages. See [here](https://meirian.gitbook.io/python/getting-started/outline) for a detailed description of the course.

# How is Python used for research? 

Watch this video!

[![https://youtu.be/VimJQ-mIAik](https://img.youtube.com/vi/VimJQ-mIAik/0.jpg)](https://youtu.be/VimJQ-mIAik)

# What will I learn? 

The content covered in this workshop can be accessed here:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.unimelb.edu.au%2Frescom-training%2Fpython%2Fintroduction-to-python-for-researchers.git/master?filepath=%2FPart%201%2F01%20-%20Introduction%20to%20Jupyter%20and%20Python.ipynb)

This workshop uses Python 3.8.6 inside the Jupyter Notebook. 

Note that the Binder can take a few attempts to open. If you are having trouble, I recommend you first disable any ad-blockers, then exit the tab and try again. Usually, Binder will open eventually if you keep trying; however, if you'd prefer to access Python using a different method then follow the below instructions. In particular, during the workshop, I recommend you use one of the online options, such as [Replit](https://repl.it).

# How can I access Python?

You can access Python in a number of ways. For recommended IDEs and text editors, visit 
[https://meirian.gitbook.io/python/getting-started/access](https://meirian.gitbook.io/python/getting-started/access). You can also [access Python online](https://meirian.gitbook.io/python/getting-started/access/online).

## How do I install Python?
Check out the [Python_Installation.md](https://github.com/resbaz/Intro_Python_Nov2017/blob/master/Python_Installation.md)
instruction file.

## IDE vs Text Editors vs Jupyter Notebook 

While we will be learning in the Jupyter environment, you may find this a bit heavy for your day-to-day coding life. 
There are a number of light-weight text editors and Integrative Development Environments (IDEs) freely available on the internet.

### What's the difference?

The primary difference is that IDEs allow you write and run your code all within the same program. On the other hand, text editors only allow you to write the code; to run them you have to use the command line. Either of these options can be more appropriate for you, depending on how you want to use them. 

IDEs:
- Spyder. This comes installed with the Anaconda package, and you should be able to find it in your system's programs list after installation
- [Pycharm](https://www.jetbrains.com/pycharm/). This one has a free community-edition license, but anybody working on research which could be patented, beware! The pycharm owners dislike the free version being used on for-profit projects, and the licensing reflects this.
- [VScode](https://code.visualstudio.com/docs/python/python-tutorial); Microsoft lightweight editor with a couple of extensions for interactive coding (and my fave!)

Text Editors:
- [Atom](https://atom.io/). Free. No ads. Allows Git/Github integration
- [Sublime Text](https://www.sublimetext.com/). Unlimited free license. Occasional pop-up ads. 

# You can still access legacy content previously presented by Jonathan Garber in Python workshops run by Research Computing Services

## Part 1 Module

|**Skill**|**Link**|**Cloud Notebook**|
|---:|---|---|
|Introduction to Jupyter Notebooks and Python|[Click Here](https://gitlab.unimelb.edu.au/rescom-training/python/introduction-to-python-for-researchers/-/blob/master/legacy/Part%201/01%20-%20Introduction%20to%20Jupyter%20and%20Python.ipynb)| [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.unimelb.edu.au%2Frescom-training%2Fpython%2Fintroduction-to-python-for-researchers.git/master/legacy?filepath=%2FPart%201%2F01%20-%20Introduction%20to%20Jupyter%20and%20Python.ipynb)|
|Lists Part a: Creating indexing and slicing lists|[Click here](https://gitlab.unimelb.edu.au/rescom-training/python/introduction-to-python-for-researchers/-/blob/master/legacy/Part%201/02a%20Creating,%20indexing,%20and%20slicing%20lists.ipynb)|[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.unimelb.edu.au%2Frescom-training%2Fpython%2Fintroduction-to-python-for-researchers.git/master/legacy?filepath=%2FPart%201%2F02a%20Creating%2C%20indexing%2C%20and%20slicing%20lists.ipynb)|
|Lists Part b: list methods|[Click Here](https://gitlab.unimelb.edu.au/rescom-training/python/introduction-to-python-for-researchers/-/blob/master/legacy/Part%201/02b%20List%20methods.ipynb)|[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.unimelb.edu.au%2Frescom-training%2Fpython%2Fintroduction-to-python-for-researchers.git/master/legacy?filepath=%2FPart%201%2F02b%20List%20methods.ipynb)|
|Dictionaries, the other way to store data | [Click here](https://gitlab.unimelb.edu.au/rescom-training/python/introduction-to-python-for-researchers/blob/master/legacy/Part%201/03%20-%20Dictionaries.ipynb) |[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.unimelb.edu.au%2Frescom-training%2Fpython%2Fintroduction-to-python-for-researchers.git/master/legacy?filepath=%2FPart%201%2F03%20-%20Dictionaries.ipynb)|

## Part 2 module

|**Skill**| **Link**|**Cloud Notebook**|
|---:|---|---|
|If statements and logic |[Click here](https://gitlab.unimelb.edu.au/rescom-training/python/introduction-to-python-for-researchers/-/blob/master/legacy/Part%202/04%20-%20Conditionals%20and%20If%20statements.ipynb) |[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.unimelb.edu.au%2Frescom-training%2Fpython%2Fintroduction-to-python-for-researchers.git/master/legacy?filepath=%2FPart%202%2F04%20-%20Conditionals%20and%20If%20statements.ipynb)|
|Introduction to loops |[Click here](https://gitlab.unimelb.edu.au/rescom-training/python/introduction-to-python-for-researchers/-/blob/master/legacy/Part%202/05%20-%20Introduction%20to%20loops.ipynb)|[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.unimelb.edu.au%2Frescom-training%2Fpython%2Fintroduction-to-python-for-researchers.git/master/legacy?filepath=%2FPart%202%2F05%20-%20Introduction%20to%20loops.ipynb)|
|Create your own functions |[Click here](https://gitlab.unimelb.edu.au/rescom-training/python/introduction-to-python-for-researchers/-/blob/master/legacy/Part%202/06%20-%20Creating%20your%20own%20functions.ipynb)|[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.unimelb.edu.au%2Frescom-training%2Fpython%2Fintroduction-to-python-for-researchers.git/master/legacy?filepath=%2FPart%202%2F06%20-%20Creating%20your%20own%20functions.ipynb)|
|A quick tour of usefull libraries for research |[Click here](https://gitlab.unimelb.edu.au/rescom-training/python/introduction-to-python-for-researchers/-/blob/legacy/master/Part%202/07%20-%20A%20tour%20of%20some%20of%20the%20cooler%20Python%20Features%20for%20researchers.ipynb)|[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.unimelb.edu.au%2Frescom-training%2Fpython%2Fintroduction-to-python-for-researchers.git/master/legacy?filepath=%2FPart%202%2F07%20-%20A%20tour%20of%20some%20of%20the%20cooler%20Python%20Features%20for%20researchers.ipynb)|

---
